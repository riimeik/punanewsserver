'use strict';

var userSockets = {};

var validateConnection = function(id, authToken, cb) {
	sql.query('SELECT * FROM user WHERE id = ? AND auth_token = ?', [id, authToken], function(err, rows, fields) {
		if(err) {
			console.error('SQL error: ', err);
		} else if(rows.length) {
			cb(true);
			return;
		}

		cb(false);
	});
}

var sql = require('mysql').createConnection(process.env.JAWSDB_MARIA_URL);
var io;

var server = require('http').createServer(function(req, res) {
	req.setEncoding('utf8');

	var emitEvents = function(data) {
		if(!data.messages) {
			return;
		}

		for(var id in data.messages) {
			if(!data.messages.hasOwnProperty(id)) {
				continue;
			}

			for(var messId in data.messages[id]) {
				if(!data.messages[id].hasOwnProperty(messId)) {
					continue;
				}

				var message = data.messages[id][messId];

				if(id == 'all') {
					io.sockets.emit(message.event, message.content);
					console.log('Sent message to all', message);
				} else if(userSockets[id]) {
					for(var i = 0; i < userSockets[id].length; i++) {
						try {
							userSockets[id][i].emit(message.event, message.content);
							console.log('Sent message to ' + id, message);
						} catch(e) {
							//
						}
					}
				}
			}
		}
	}

	var body = '';

	req.on('data', function(data) {
		body += data;
	});

	req.on('end', function() {
		console.log('New http request');

		var data = {};

		try {
			data = JSON.parse(body);
		} catch(e) {
			console.error("Couldn't parse JSON: ", body);
			res.end('Invalid JSON format');
			return;
		}

		var authToken = data.authToken || '';
		var parts = (authToken.indexOf(':') >= 0 ? authToken : ':').split(':');

		validateConnection(parts[0], parts[1], function(status) {
			if(status && parts[0] == 1) {
				emitEvents(data);
				res.end('OK');
			} else {
				res.end('Authentication failed');
			}
		});
	});
});

 io = require('socket.io')(server);

io.on('connection', function(socket) {
	var authToken = socket.handshake.query.authToken || '';
	var parts = (authToken.indexOf(':') >= 0 ? authToken : ':').split(':');

	validateConnection(parts[0], parts[1], function(status) {
		if(status) {
			if(!userSockets.hasOwnProperty(parts[0])) {
				userSockets[parts[0]] = [];
			}

			userSockets[parts[0]].push(socket);
			console.log('New ws client: ' + parts[0]);
		} else {
			console.log('New ws guest client');
		}
	});

	socket.on('disconnect', function() {
		//
	})
});

sql.connect(function(err) {
	if(err) {
		console.error('SQL error: ', err);
		return;
	}

	console.log('DB connection established');
});

server.listen(process.env.PORT, function() {
	console.log('Started listening..');
});